#!/bin/bash
cd /output

npm i puppeteer
cp /script-a.js /output/script-a.js
cp /script-d.js /output/script-d.js
cp /script-s.js /output/script-s.js
cp /script-r.js /output/script-r.js

if [ -e a.txt ]; then
node script-a.js
fi    

if [ -e s.txt ]; then
node script-s.js
fi    

if [ -e d.txt ]; then
node script-d.js
fi    

if [ -e r.txt ]; then
node script-r.js
fi    
