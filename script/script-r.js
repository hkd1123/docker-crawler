const fs = require('fs');
const aurl = require('url');
const assert = require('assert');
const puppeteer = require('puppeteer');
const request = require('https');

//const PREFS = ['hokkaido','tohoku','kanto','tokai','hokuriku','kansai','chugoku','shikoku','kyushu']
const PREFS = ['hokkaido','aomori','iwate','miyagi','akita',
'yamagata','fukushima','ibaraki','tochig','gunma','saitama',
'chiba','tokyo','kanagawa',
'yamanashi','niigata','nagano','toyama','ishikawa','fukui',
'gifu','shizuoka',
'aichi','mie','shiga','kyoto','osaka','hyogo','nara',
'wakayama','tottori','shimane','okayama','hiroshima',
'yamaguchi','tokushima',
'kagawa','ehime','kochi','fukuoka','saga',
'nagasaki','kumamoto','oita','miyazaki','kagoshima','okinawa'
]
const TOP_URL = 'https://mobile.rakuten.co.jp/shop/'

try {


(async() => {

  // const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const browser = await puppeteer.launch({executablePath: '/usr/bin/chromium-browser', args: ['--no-sandbox', '--headless', '--disable-gpu']});
  //const browser = await puppeteer.launch();
  const page = await browser.newPage();

  /*** 1 home ***/
  /* 非同期に取得するコンテンツの表示のためには、networkidleのtimeoutを十分大きくとる */
  await page.goto(TOP_URL, {waitUntil: 'networkidle2'}); //default 1000
  /* 画面をフルサイズで撮る */
  await page.screenshot({path: 'r-home1.png', fullPage: true});


  await page.title().then( text => console.log("############# TOP text="+text) );
  
  const fnPagers = async function(page) {
    const data = await page.evaluate((selector) => {
      const sel = document.querySelector(selector)
      if (sel == null) return null
      return {
          href: document.querySelector(selector).href,
          textContent: document.querySelector(selector).textContent,
          innerHTML: document.querySelector(selector).innerHTML
      };
    }, '.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next');

    if (data == null) return false

    // pagerの次へをクリック
    page.click('.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next')
    await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    return true

  }

  //一覧ページからショップ詳細のリンクを抽出
  const fnShopdata = async function(page, shopURL, url) {
    console.log("fnShopdata working.....");

    let shopdatas = await page.$$eval('.sec-cmn-lv4 th[data-headers="cell-name-01"] p a.ico-cmn-arrow', (list,surl) => {
      var datas=[];
      //console.log(lurl.parse('https://yahoo.co.jp/?hoge=234&eeee=2efeo'));
      for (let i = 0; i < list.length; i++) {
        var data = {
          urlquery: '',
          pref: surl.pref,
          href: list[i].href + "&sn=on",
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML
        };
        datas.push(data);
      }
      return datas;
    },shopURL);

    var datas=[];
    for (let i = 0; i < shopdatas.length; i++) {
      var qurl = url.parse(shopdatas[i].href,true)
      var queryarray = qurl.query

      var data = {
        urlquery: qurl.query,
        pref: shopdatas[i].pref,
        href: shopdatas[i].href,
        textContent: shopdatas[i].textContent,
        innerHTML: shopdatas[i].innerHTML
      }; 
      datas.push(data)
    }
    return datas

  }

  //詳細ページから項目データを抽出
  const fnShopDetail = async function(page, shopURL, url, areq) {
    console.log("fnShopdetail working.....");

    // let maplocation = await page.$$eval('section.sec-cmn-lv2 input', (list,surl) => {
    //   var datas=[];
    //   if (list.length == 0) return false;
    //   for (let i = 0; i < list.length; i++) {
    //     var data =  list[i].value;
    //     datas.push(data);
    //   }
    //   return datas;
    // },shopURL);
    // console.log(maplocation);
    let maplocation = "";

    let opentime = await page.$$eval('.info div.short-view dl dd', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas[0];
    },shopURL);
    console.log(opentime);
    shopURL.contents[4] = opentime;

    //サービス情報
    //let bulkdatarow2 = await page.$$eval('div.table-section.table-grid.toolTip-section.search-data.margin-t-20 tbody tr td.bgColor-lightGray', (list,surl) => {
    try {


    let bulkdatarow2 = await page.$$eval('ul.list-service li img', (list,surl) => {
      var datas=[];
      var services = {};
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].alt;
        services[data] = true;
      }
      for (key in services) {
        datas.push(key);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarow2);
    console.log(bulkdatarow2.length);

    console.log(shopURL);

    var adrr = encodeURIComponent(shopURL.prefa)
    var apiurl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + adrr + "&key=AIzaSyBgxuPGugOTNxaEl8PF3Us0t513VRfVqCg";
    // AIzaSyCW5C3I9oX-iSaKnc_kknhdS1AhNL8dupc
    // AIzaSyBgxuPGugOTNxaEl8PF3Us0t513VRfVqCg

    const httpGet = (url,http) => {
      return new Promise((resolve, reject) => {
        http.get(url, res => {
          res.setEncoding('utf8');
          let body = ''; 
          res.on('data', chunk => body += chunk);
          res.on('end', () => resolve(body));
        }).on('error', reject);
      });
    };

    let body = await httpGet(apiurl,areq);
    var ret
    var loc
    try{
      ret = JSON.parse(body);
      loc = ret.results[0].geometry.location
    } catch (err) {
      console.error(err)
      loc = {lat : '0', lng : '0'}
    }



    shopURL.lat = loc.lat
    shopURL.lon = loc.lng

    for (let i = 0; i < bulkdatarow2.length; i++) {
      shopURL.contents.push( bulkdatarow2[i] + ":" + "○")
    }    
    console.log(shopURL);

    }catch(err){
      
    }

    return shopURL

  }

  const fnConvArray = function convArray(dat) {
    let ret = [];

    ret.push(dat.name);
    ret.push(dat.prefn);
    ret.push(dat.prefa);
    ret.push(dat.href);
    ret.push(dat.id);
    ret.push(dat.contents[0]);
    ret.push(dat.contents[1]);
    ret.push(dat.contents[2]);
    ret.push(dat.lat);
    ret.push(dat.lon);
    ret.push("6200007");
    ret.push(dat.location);
    for(let i = 3; i < dat.contents.length; i++) {
      ret.push(dat.contents[i]);
    }

    return ret;
  }

  const fnWriteData = function appendFile(path, data) {
    reccsv = "";
    for (let i=0; i < data.length; i++) {
      if (reccsv === "") {
        reccsv = '"' + data[i] + '"';
      }else{
        reccsv = reccsv + "," + '"' + data[i] + '"';
      }
    }

    fs.appendFile(path, reccsv + "\n", function (err) {
      if (err) {
          throw err;
      }
    });
  }        

  let areadatas
  let shopdetails=[]

  for (currentURL of PREFS) {
    await page.goto(TOP_URL, {waitUntil: 'networkidle2'}); //default 1000

    const allResultsSelector = '#map a[href="#' +currentURL+'"]';
    //const allResultsSelector2 = '.sec-cmn-lv3 .mod-shop-map.only-pc .item.s-'+currentURL+' .list a';
    const areas = await page.$(allResultsSelector)
    await areas.click();
    //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});
    //console.log(areas);

    await page.title().then( text => console.log("############# "+currentURL+" text="+text) );
    await page.screenshot({path: currentURL + '-r-home2.png', fullPage: true})

    //const areaname = await page.$('#area_name')

    const areaname = await page.evaluate((selector) => {
      const sel = document.querySelector(selector)
      if (sel == null) return null
      return document.querySelector(selector).textContent
    }, '#area_name');

    console.log(areaname)
    // <div class="shop-name-col">\n        <a href="/shop/ksdenki_sapporoasabu/" class="shop-name">ケーズデンキ札幌麻生店</a>\n      </div>\n      <div class="detail-col">\n        <p class="address">〒001-0036<br>北海道札幌市北区北36条西10丁目1-5</p>\n      <div class="icon-list"></div></div>\n    ',
    //   '\n      <div class="shop-name-col">\n        <a href="/shop/ksdenki_hakodatehonten/" class="shop-name">ケーズデンキ 函館本店</a>\n        <span class="new">New</span>\n        <p class="info-open">5月1日(火)オープン</p>\n      </div>\n      <div class="detail-col">\n        <p class="address">〒041-0806<br>北海道函館市美原3丁目52-1</p>\n      <div class="icon-list"></div></div>\n    ',
    //   '\n      <div class="shop-name-col">\n        <a href="/shop/geo_hakodateshouwaten/" class="shop-name">ゲオ 函館昭和店</a>\n      </div>\n      <div class="detail-col">\n        <p class="address">〒041-0812<br>北海道函館市昭和3丁目24-18</p>\n      <div class="icon-list"></div></div>\n    ',
    const fnGetShoplist = async function(tag) {
      let shoplist1 = await page.$$eval('#output_html .shop-area .shop-list li.' +tag+ ' .shop-name', (list, prefn) => {
        var datas=[]
        for (let i = 0; i < list.length; i++) {
          // console.log(list[i].innerHTML)
          //const shopname = list[i].$(".shop-name")
          var ids = list[i].href.split('/')

          const dat = {
            name: list[i].innerText,
            lat: "",
            lon: "",
            location: [],        
            contents: [
                 "", // postal: "",
                 "", // addr: "",
                 "", // tel1: "",
                 "", // tel2: "",
                 "", // open: "",
                 "" // holiday: "",
            ],
            prefn: prefn,
            prefa: "",
            href: list[i].href,
            id: ids[ids.length - 2]

          }
          datas.push(dat)

        };
        return datas;
      }, areaname);

      shoplist1 = await page.$$eval('#output_html .shop-area .shop-list li.' +tag+ ' p.address', (list, shoplist) => {
        var datas=[]
        for (let i = 0; i < list.length; i++) {
          var dat = list[i].innerText.split('\n')
          shoplist[i].contents[0] = dat[0].split("〒")[1]
          shoplist[i].contents[1] = dat[1]

          shoplist[i].prefa = dat[1].split(shoplist[i].prefn)[1]
          datas.push(shoplist[i])

        };

        return datas;
      },shoplist1);

      return shoplist1;
    }

    let shoplist=[]

    try {
      ret = await fnGetShoplist("shop-list-full")
      console.log(ret)
      if (ret.length > 0) {
        shoplist = shoplist.concat(ret);

      }

    }catch(err){

    }


    try {

      ret = await fnGetShoplist("shop-list-simple")
      console.log(ret)
      if (ret.length > 0) {
        shoplist = shoplist.concat(ret);
        
      }

    }catch(err){

    }


    const sleepNotify = () => {
      console.log('wait sleeping')
    }
    const sleep = (waitSeconds, someFunction) => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(someFunction())
        }, waitSeconds * 1000)
      })  
    }

    for (shopdURL of shoplist) {
      sleep(3,sleepNotify)
      let gotopage=false
      while (!gotopage) {
        try {
          await page.goto(shopdURL.href);
          gotopage = true;
        }catch(err){
          console.log(err)
        }

      }
      console.log('before');

      console.log(shopdURL);
      // await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});
      // await page.screenshot({path: shopdURL.pref + '_' + shopdURL.textContent + '_' + '_detail.png', fullPage: true})

      let detail = await fnShopDetail(page,shopdURL,aurl,request)
      if (detail !== false) {
        fnWriteData("./test-r.csv",fnConvArray(detail));
      }

    }


     // contents:
     //  [ '001-0908',
     //    '北海道 札幌市北区 新琴似８条１２丁目２－４',
     //    '0800-700-1923',
     //    '011-763-7007',
     //    '10:00～20:00',
     //    '毎月第3水曜日※3月、9月は休まず営業いたします。' ],
     // prefn: '北海道',
     // prefa: '札幌市北区',
     // pref: 'hokkaido',
     // href: 'https://www.au.com/aushop/shop.D-00058/',
     // id: 'D-00058' } }
  }

  browser.close();

  //assert(fs.existsSync('home1.png'));

  console.log('### Helooooooo');
})();
} catch (err) {
  console.error(err)
}





