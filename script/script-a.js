const fs = require('fs');
const aurl = require('url');
const assert = require('assert');
const puppeteer = require('puppeteer');

const PREFS = ['hokkaido','tohoku','kanto','tokai','hokuriku','kinki','chugoku','shikoku','kyushu','okinawa']
//const PREFS = ['hokkaido']
const TOP_URL = 'https://www.au.com/aushop/'

try {


(async() => {

  // const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const browser = await puppeteer.launch({executablePath: '/usr/bin/chromium-browser', args: ['--no-sandbox', '--headless', '--disable-gpu']});
  //const browser = await puppeteer.launch();
  const page = await browser.newPage();

  /*** 1 home ***/
  /* 非同期に取得するコンテンツの表示のためには、networkidleのtimeoutを十分大きくとる */
  await page.goto(TOP_URL, {waitUntil: 'networkidle2'}); //default 1000
  /* 画面をフルサイズで撮る */
  await page.screenshot({path: 'a-home1.png', fullPage: true});


  await page.title().then( text => console.log("############# TOP text="+text) );

  const fnConvArray = function convArray(dat) {
    let ret = [];
    ret.push(dat.name);
    ret.push(dat.prefn);
    ret.push(dat.prefa);
    ret.push(dat.href);
    ret.push(dat.id);
    ret.push(dat.contents[0]);
    ret.push(dat.contents[1]);
    ret.push(dat.contents[2]);
    ret.push(dat.lat);
    ret.push(dat.lon);
    ret.push("6200001");
    ret.push(dat.location);
    for(let i = 3; i < dat.contents.length; i++) {
      ret.push(dat.contents[i]);
    }

    return ret;
  }

  const fnWriteData = function appendFile(path, data) {
    reccsv = "";
    for (let i=0; i < data.length; i++) {
      if (reccsv === "") {
        reccsv = '"' + data[i] + '"';
      }else{
        reccsv = reccsv + "," + '"' + data[i] + '"';
      }
    }

    fs.appendFile(path, reccsv + "\n", function (err) {
      if (err) {
          throw err;
      }
    });
  }       
  
  const fnPagers = async function(page) {
    const data = await page.evaluate((selector) => {
      const sel = document.querySelector(selector)
      if (sel == null) return null
      return {
          href: document.querySelector(selector).href,
          textContent: document.querySelector(selector).textContent,
          innerHTML: document.querySelector(selector).innerHTML
      };
    }, '.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next');

    if (data == null) return false

    // pagerの次へをクリック
    page.click('.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next')
    await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    return true

  }

  //一覧ページからショップ詳細のリンクを抽出
  const fnShopdata = async function(page, shopURL, url) {
    console.log("fnShopdata working.....");

    let shopdatas = await page.$$eval('.sec-cmn-lv4 th[data-headers="cell-name-01"] p a.ico-cmn-arrow', (list,surl) => {
      var datas=[];
      //console.log(lurl.parse('https://yahoo.co.jp/?hoge=234&eeee=2efeo'));
      for (let i = 0; i < list.length; i++) {
        var data = {
          urlquery: '',
          pref: surl.pref,
          href: list[i].href + "&sn=on",
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML
        };
        datas.push(data);
      }
      return datas;
    },shopURL);

    var datas=[];
    for (let i = 0; i < shopdatas.length; i++) {
      var qurl = url.parse(shopdatas[i].href,true)
      var queryarray = qurl.query

      var data = {
        urlquery: qurl.query,
        pref: shopdatas[i].pref,
        href: shopdatas[i].href,
        textContent: shopdatas[i].textContent,
        innerHTML: shopdatas[i].innerHTML
      }; 
      datas.push(data)
    }
    return datas

  }

  //詳細ページから項目データを抽出
  const fnShopDetail = async function(page, shopURL, url) {
    console.log("fnShopdetail working.....");

    let maplocation = await page.$$eval('a.btnElem.btn-basic.padding-l-30.padding-r-30.map-apps-pc', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].href;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(maplocation);

    var qurl = url.parse(maplocation[0],true)
    var queryarray = qurl.query

    let location = await page.$$eval('.layout-display a.link-text.txtSize-small.store-closest-station', (list,surl) => {
      var datas=[];
      if (list.length == 0) return [];
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent.split('\n');
        if (data.length > 1) {
          data = (data[1] + "")
          data = data.replace(/^\s+|\s+$/g,'');
        } else {
          data = []
        }
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(location);

    //サービス情報
    //let bulkdatarow2 = await page.$$eval('div.table-section.table-grid.toolTip-section.search-data.margin-t-20 tbody tr td.bgColor-lightGray', (list,surl) => {
    let bulkdatarow2 = await page.$$eval('div.table-section.table-grid.toolTip-section.search-data.margin-t-20 tbody tr td:nth-child(1)', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent.split('\n')[0];
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarow2);

    let bulkdatarowd2 = await page.$$eval('div.table-section.table-grid.toolTip-section.search-data.margin-t-20 tbody tr td:nth-child(2)', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent.split('\n')[1].replace(/^\s+|\s+$/g,'');
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarowd2);


    let strId = shopURL.id[shopURL.id.length-2].split(".")[1]
    let shopname_pos = await page.$$eval('#shop_' + strId + ' section.margin-t-40.margin-r-0.margin-b-0.margin-l-0 table.tableElem.margin-t-20 tbody tr td[colspan]', (list,surl) => {
      let datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data = list[i].textContent.split('\n')[0];
        switch (i){
        case 0: //postal no
          break;
        case 1: //address海道 札幌市厚別区 厚別西
          surl.prefn = list[i].textContent.split(' ')[0];
          surl.prefa = list[i].textContent.split(' ')[1];
          break;
        case 2: //tel1
          if(data.length > 13) {
            data = data.slice(0, data.length/2)
          }
          break;
        case 3: //tel2
          if(data.length > 13) {
            data = data.slice(0, data.length/2)
          }
          break;
        default:
          //no modify
          break;
        }

        datas.push(data);
      }
      var ret = {
        r: datas, 
        s: surl
      }
      return ret;
    },shopURL);


    for (let i = 0; i < bulkdatarow2.length; i++) {
      shopname_pos.r.push( bulkdatarow2[i] + ":" + bulkdatarowd2[i])
    }

    console.log(shopname_pos);

    let detail =  {
      shopdetail: {
        name: shopURL.textContent, 
        contents: shopname_pos.r, 
        prefn: shopname_pos.s.prefn, 
        prefa:shopname_pos.s.prefa, 
        pref: shopURL.pref, 
        href: shopURL.href,
        lat: queryarray['lat'],
        lon: queryarray['lon'],
        location: location,        
        id: strId
      }
    }
    console.log(detail);
    return detail

  }


  let shopdetails=[]

  for (currentURL of PREFS) {
    console.log("fnShoparea working.....");

    await page.goto(TOP_URL + currentURL + '/');

    const areatitle = await page.title().then( text => {
      console.log("############# text="+text)
      return text;
    });
    // await page.screenshot({path: currentURL + areatitle + 'home2.png', fullPage: true})
    let shopsdata=[]

    let areadatas = await page.$$eval('section.cardBox-body div.cardBox-bodyCont a', (list,curl) => {
      var datas=[];
      for (let i = 0; i < list.length; i++) {
        let urlq = list[i].href.split("/");

        var data = {
          pref: curl,
          href: list[i].href,
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML,
          id: urlq
        };
        datas.push(data);
      }
      return datas;
    },currentURL);

    shopsdata = shopsdata.concat(areadatas)

    //await page.goBack()
    //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    console.log("some items some attributes using $$eval");
    console.log(shopsdata);

    const sleepNotify = () => {
      console.log('wait sleeping')
    }
    const sleep = (waitSeconds, someFunction) => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(someFunction())
        }, waitSeconds * 1000)
      })  
    }

    for (shopdURL of shopsdata) {
      sleep(3,sleepNotify)
      let gotopage=false
      while (!gotopage) {
        try {
          await page.goto(shopdURL.href);
          gotopage = true;
        }catch(err){
          console.log(err)
        }

      }
      // await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});
      // await page.screenshot({path: shopdURL.pref + '_' + shopdURL.textContent + '_' + '_detail.png', fullPage: true})

      const detail = await fnShopDetail(page,shopdURL,aurl)
      if (detail !== false) {
        fnWriteData("./test-a.csv",fnConvArray(detail.shopdetail));
        shopdetails.push(detail);
      }

    }

  }
  // //console.log(shopdetails);

  browser.close();

  //assert(fs.existsSync('home1.png'));

  console.log('### Helooooooo');
})();
} catch (err) {
  console.error(err)
}



