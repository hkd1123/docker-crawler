const fs = require('fs');
const aurl = require('url');
const assert = require('assert');
const puppeteer = require('puppeteer');
const request = require('https');

//const PREFS = ['hokkaido','tohoku','kanto','tokai','hokuriku','kansai','chugoku','shikoku','kyushu']
const PREFS = ['shikoku','kyushu']
const TOP_URL = 'https://www.nttdocomo.co.jp/support/shop/'

try {


(async() => {

  // const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const browser = await puppeteer.launch({executablePath: '/usr/bin/chromium-browser', args: ['--no-sandbox', '--headless', '--disable-gpu']});
  //const browser = await puppeteer.launch();
  const page = await browser.newPage();

  /*** 1 home ***/
  /* 非同期に取得するコンテンツの表示のためには、networkidleのtimeoutを十分大きくとる */
  await page.goto(TOP_URL, {waitUntil: 'networkidle2'}); //default 1000
  /* 画面をフルサイズで撮る */
  await page.screenshot({path: 'dc-home1.png', fullPage: true});


  await page.title().then( text => console.log("############# TOP text="+text) );

  const fnConvArray = function convArray(dat) {
    let ret = [];
    ret.push(dat.name);
    ret.push(dat.prefn);
    ret.push(dat.prefa);
    ret.push(dat.href);
    ret.push(dat.id);
    ret.push(dat.contents[0]);
    ret.push(dat.contents[1]);
    ret.push(dat.contents[2]);
    ret.push(dat.lat);
    ret.push(dat.lon);    
    ret.push("6200003");
    ret.push(dat.location);    

    for(let i = 3; i < dat.contents.length; i++) {
      ret.push(dat.contents[i]);
    }

    return ret;
  }

  const fnWriteData = function appendFile(path, data) {
    reccsv = "";
    for (let i=0; i < data.length; i++) {
      if (reccsv === "") {
        reccsv = '"' + data[i] + '"';
      }else{
        reccsv = reccsv + "," + '"' + data[i] + '"';
      }
    }

    fs.appendFile(path, reccsv + "\n", function (err) {
      if (err) {
          throw err;
      }
    });
  }  
  
  const fnPagers = async function(page) {
    const data = await page.evaluate((selector) => {
      const sel = document.querySelector(selector)
      if (sel == null) return null
      return {
          href: document.querySelector(selector).href,
          textContent: document.querySelector(selector).textContent,
          innerHTML: document.querySelector(selector).innerHTML
      };
    }, '.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next');

    if (data == null) return false

    // pagerの次へをクリック
    page.click('.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next')
    await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    return true

  }

  //一覧ページからショップ詳細のリンクを抽出
  const fnShopdata = async function(page, shopURL, url) {
    console.log("fnShopdata working.....");

    let areastr = await page.$$eval('#region_name h2.tit-cmn-lv2', (list) => {
      var datas=[];
      for (let i = 0; i < list.length; i++) {
        var data = list[i].textContent.split('：')[1].split(' ')[1]
        datas.push(data);
      }
      return datas[0];
    });
//都道府県から検索：北海道 札幌市厚別区

    let shopdatas = await page.$$eval('.sec-cmn-lv4 th[data-headers="cell-name-01"] p a.ico-cmn-arrow', (list,surl) => {
      var datas=[];
      //console.log(lurl.parse('https://yahoo.co.jp/?hoge=234&eeee=2efeo'));
      for (let i = 0; i < list.length; i++) {
        var data = {
          urlquery: '',
          pref: surl.pref,
          href: list[i].href + "&sn=on",
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML
        };
        datas.push(data);
      }
      return datas;
    },shopURL);

    var datas=[];
    for (let i = 0; i < shopdatas.length; i++) {
      var qurl = url.parse(shopdatas[i].href,true)
      var queryarray = qurl.query

      var data = {
        urlquery: qurl.query,
        pref: shopdatas[i].pref,
        prefn: shopdatas[i].pref,
        prefa: areastr,
        href: shopdatas[i].href,
        textContent: shopdatas[i].textContent,
        innerHTML: shopdatas[i].innerHTML
      }; 
      datas.push(data)
    }
    return datas

  }

  //詳細ページから項目データを抽出
  const fnShopDetail = async function(page, shopURL, url, areq) {
    console.log("fnShopdetail working.....");

    let maplocation = await page.$$eval('section.sec-cmn-lv2 input', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].value;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(maplocation);

    // var qurl = url.parse(maplocation[0],true)
    // var queryarray = qurl.query

    //locationなしのためブランク
    let location = [];


    //サービス情報
    //let bulkdatarow2 = await page.$$eval('div.table-section.table-grid.toolTip-section.search-data.margin-t-20 tbody tr td.bgColor-lightGray', (list,surl) => {
    let bulkdatarow2 = [];
    try{

      bulkdatarow2 = await page.$$eval('tbody tr td ul.mod-cmn-ico-list li img', (list,surl) => {
        var datas=[];
        if (list.length == 0) return false;
        for (let i = 0; i < list.length; i++) {
          var data =  list[i].alt;
          datas.push(data);
        }
        return datas;
      },shopURL);
      console.log(bulkdatarow2);
    } catch (err) {
      console.error(err)
    }

    // let bulkdatarowd2 = await page.$$eval('div.table-section.table-grid.toolTip-section.search-data.margin-t-20 tbody tr td:nth-child(2)', (list,surl) => {
    //   var datas=[];
    //   if (list.length == 0) return false;
    //   for (let i = 0; i < list.length; i++) {
    //     var data =  list[i].textContent.split('\n')[1].replace(/^\s+|\s+$/g,'');
    //     datas.push(data);
    //   }
    //   return datas;
    // },shopURL);
    // console.log(bulkdatarowd2);


    let shopname_pos = await page.$$eval('.sec-cmn-lv2 .mod-cmn-reverse div.col2 figure.fig-cmn-table table.table-cmn tbody tr td.s-ta-l.s-va-t', (list,surl) => {
      var datas=[];
      // [ '',
      //   '〒004-0041',
      //   '北海道札幌市厚別区大谷地東3丁目3-20キャポ大谷地 1階',
      //   'TEL：',
      //   '0120-782-360',
      //   '0120-782-360',
      //   'TEL：',
      //   '011-375-6702',
      //   '011-375-6702',
      //   '',
      //   '',
      //   '' ],
      // [ '', '午前10時～午後8時', '', '' ],
      // [ '', '無休', '' ] ],      
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data = list[i].textContent.split('\n');
        switch (i){
        case 0: //postal and address
          for (let i2 = 0; i2 < data.length; i2++) {
            if (i2 === 1 ) {
              datas.push(data[i2].split('〒')[1]);
            }
            
            if (i2 === 2 || i2 === 4 || i2 === 7) {
              datas.push(data[i2]);
            }
          }
          break;
        case 1: //opentime
          for (let i2 = 0; i2 < data.length; i2++) {
            if (i2 === 1) {
              datas.push(data[i2]);
            }
          }
          break;
        case 2: //holiday
          for (let i2 = 0; i2 < data.length; i2++) {
            if (i2 === 1) {
              datas.push(data[i2]);
            }
          }
          break;
        default:
          //no modify
          break;
        }

      }

      surl.prefn = surl.pref;
      var ret = {
        r: datas, 
        s: surl
      }
      return ret;
    },shopURL);



    var adrr = encodeURIComponent(shopname_pos.r[1])
    var apiurl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + adrr + "&key=AIzaSyBgxuPGugOTNxaEl8PF3Us0t513VRfVqCg";
    // AIzaSyCW5C3I9oX-iSaKnc_kknhdS1AhNL8dupc
    // AIzaSyBgxuPGugOTNxaEl8PF3Us0t513VRfVqCg
    const httpGet = (url,http) => {
      return new Promise((resolve, reject) => {
        http.get(url, res => {
          res.setEncoding('utf8');
          let body = ''; 
          res.on('data', chunk => body += chunk);
          res.on('end', () => resolve(body));
        }).on('error', reject);
      });
    };

    let body = await httpGet(apiurl,areq);
    var ret
    var loc
    try{
      ret = JSON.parse(body);
      loc = ret.results[0].geometry.location
    } catch (err) {
      console.error(err)
      loc = {lat : '0', lng : '0'}
    }



    // console.log(apiurl);
    // var loc
    // let reqc = areq.get(apiurl, function(res) {

    //     var body = '';
    //     res.setEncoding('utf8');

    //     res.on('data', function(chunk) {
    //         body += chunk;
    //     });

    //     res.on('end', function(res) {
    //         ret = JSON.parse(body);
    //         res = ret.results[0].geometry.location
    //         //console.log(ret.results[0].geometry);
    //     });

    // }).on('error', function(e) {
    //     console.log(e.message);
    // });

    console.log(body);

    for (let i = 0; i < bulkdatarow2.length; i++) {
      shopname_pos.r.push( bulkdatarow2[i] + ":" + "○")
    }    
    console.log(shopname_pos);

    let detail =  {
      shopdetail: {
        name: shopURL.textContent, 
        contents: shopname_pos.r, 
        prefn: shopname_pos.s.prefn, 
        prefa:shopname_pos.s.prefa, 
        pref: shopURL.pref, 
        href: shopURL.href,
        lat: loc.lat,
        lon: loc.lng,        
        location: location,        
        id: shopURL.urlquery.id
      }
    }
    console.log(detail);
    return detail

  }


  let areadatas=[]
  let shopdetails=[]

  for (currentURL of PREFS) {
    console.log("retreive pref scraping.......");
    let allResultsSelector = '.sec-cmn-lv3 .mod-shop-map.only-pc .item.s-'+currentURL+' .list a.btn-cmn';
    //const allResultsSelector2 = '.sec-cmn-lv3 .mod-shop-map.only-pc .item.s-'+currentURL+' .list a';
    let areas = await page.$$eval(allResultsSelector, list => {
      let areas=[];
      for (let i = 0; i < list.length; i++) {
        const area = {
          href: list[i].href,
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML
        };
        areas.push(area);
      }
      return areas;
    });
    console.log(areas);

    for (areaURL of areas) {
      page.goto(areaURL.href);
      await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

      await page.title().then( text => console.log("############# "+areaURL.textContent+" text="+text) );
      // await page.screenshot({path: currentURL + areaURL.textContent + 'home2.png', fullPage: true})

      let areadata = await page.$$eval('.sec-cmn-lv3 a', (list,curl,acxt) => {
        var datas=[];
        for (let i = 0; i < list.length; i++) {
          var data = {
            prefa: curl,
            pref: acxt,
            href: list[i].href,
            textContent: list[i].textContent,
            innerHTML: list[i].innerHTML
          };
          datas.push(data);
        }
        return datas;
      },currentURL,areaURL.textContent);
      
      areadatas = areadatas.concat(areadata)

      await page.goBack()
      //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    }
  }
  console.log("some items some attributes using $$eval");
  console.log(areadatas);

  let shopsdata=[]
  for (shopsURL of areadatas) {
    await page.goto(shopsURL.href);
    //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    let title = await page.title().then( text => {console.log("############# "+shopsURL.textContent+" text="+text); return text;} );
    let titles = title.split(' ');
    shopsURL.pref = titles[0];
    // await page.screenshot({path: shopsURL.prefa + shopsURL.pref + ' ' + shopsURL.textContent + 'home3.png', fullPage: true})
    console.log('### start page crawling');
    shopsdata = await fnShopdata(page, shopsURL, aurl)



    let ret = true
    let pidx = 4
    while(ret) {
      ret = await fnPagers(page)
      console.log('### next page crawling ' + ret);
      if (ret) {
        //await page.screenshot({path: shopsURL.prefa + shopsURL.pref + ' ' + shopsURL.textContent + 'home'+pidx+'.png', fullPage: true})
        pidx++
        const shopsdata2 = await fnShopdata(page, shopsURL, aurl)
        shopsdata = shopsdata.concat(shopsdata2)
      }

    }
    console.log('### shops data ');
    console.log(shopsdata);



    for (shopdURL of shopsdata) {
      await page.goto(shopdURL.href);
      //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});
      // await page.screenshot({path: shopdURL.pref + '_' + shopdURL.textContent + '_' + shopdURL.urlquery.id + '_detail.png', fullPage: true})

      const detail = await fnShopDetail(page,shopdURL,aurl,request)
      if (detail !== false) {
        fnWriteData("./test-d.csv",fnConvArray(detail.shopdetail));
        shopdetails.push(detail);
      }

    }

  }
  //console.log(shopdetails);

  browser.close();

  //assert(fs.existsSync('home1.png'));

  console.log('### Helooooooo');
})();
} catch (err) {
  console.error(err)
}



