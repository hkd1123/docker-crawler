const fs = require('fs');
const aurl = require('url');
const assert = require('assert');
const puppeteer = require('puppeteer');

const PREFS = ['01','02','03','04','05','06','07','08','09',
'10','11','12','13','14','15','16','17','18','19',
'20','21','22','23','24','25','26','27','28','29',
'30','31','32','33','34','35','36','37','38','39',
'40','41','42','43','44','45','46','47'
]
//const PREFS = ['13']
const TOP_URL = 'https://www.softbank.jp/shop/'

try {


(async() => {

  // const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const browser = await puppeteer.launch({executablePath: '/usr/bin/chromium-browser', args: ['--no-sandbox', '--headless', '--disable-gpu']});
  //const browser = await puppeteer.launch();
  const page = await browser.newPage();

  /*** 1 home ***/
  /* 非同期に取得するコンテンツの表示のためには、networkidleのtimeoutを十分大きくとる */
  await page.goto(TOP_URL, {waitUntil: 'networkidle2'}); //default 1000
  /* 画面をフルサイズで撮る */
  await page.screenshot({path: 'sb-home1.png', fullPage: true});


  await page.title().then( text => console.log("############# TOP text="+text) );

  const fnConvArray = function convArray(dat) {
    let ret = [];
    ret.push(dat.name);
    ret.push(dat.prefn);
    ret.push(dat.prefa);
    ret.push(dat.href);
    ret.push(dat.id);
    ret.push(dat.contents[0]);
    ret.push(dat.contents[1]);
    ret.push(dat.contents[2]);
    ret.push(dat.lat);
    ret.push(dat.lon);
    ret.push("6200002");
    ret.push(dat.location);
    for(let i = 3; i < dat.contents.length; i++) {
      ret.push(dat.contents[i]);
    }

    return ret;
  }

  const fnWriteData = function appendFile(path, data) {
    reccsv = "";
    for (let i=0; i < data.length; i++) {
      if (reccsv === "") {
        reccsv = '"' + data[i] + '"';
      }else{
        reccsv = reccsv + "," + '"' + data[i] + '"';
      }
    }

    fs.appendFile(path, reccsv + "\n", function (err) {
      if (err) {
          throw err;
      }
    });
  }       
  
  const fnPagers = async function(page) {
    const data = await page.evaluate((selector) => {
      const sel = document.querySelector(selector)
      if (sel == null) return null
      return {
          href: document.querySelector(selector).href,
          textContent: document.querySelector(selector).textContent,
          innerHTML: document.querySelector(selector).innerHTML
      };
    }, '.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next');

    if (data == null) return false

    // pagerの次へをクリック
    page.click('.nav-cmn-pager.js-pager a.ico-cmn-arrow.s-next')
    await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    return true

  }

  //一覧ページからショップ詳細のリンクを抽出
  const fnShopdata = async function(page, shopURL, url) {
    console.log("fnShopdata working.....");

    let shopdatas = await page.$$eval('.sec-cmn-lv4 th[data-headers="cell-name-01"] p a.ico-cmn-arrow', (list,surl) => {
      var datas=[];
      //console.log(lurl.parse('https://yahoo.co.jp/?hoge=234&eeee=2efeo'));
      for (let i = 0; i < list.length; i++) {
        var data = {
          urlquery: '',
          pref: surl.pref,
          href: list[i].href + "&sn=on",
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML
        };
        datas.push(data);
      }
      return datas;
    },shopURL);

    var datas=[];
    for (let i = 0; i < shopdatas.length; i++) {
      var qurl = url.parse(shopdatas[i].href,true)
      var queryarray = qurl.query

      var data = {
        urlquery: qurl.query,
        pref: shopdatas[i].pref,
        href: shopdatas[i].href,
        textContent: shopdatas[i].textContent,
        innerHTML: shopdatas[i].innerHTML
      }; 
      datas.push(data)
    }
    return datas

  }

  //詳細ページから項目データを抽出
  // [ 'ソフトバンクさっぽろ地下街ポールタウン',
  //   '株式会社ウイング・アール',
  //   '〒060-0063北海道札幌市中央区南三条西４丁目１２番地　札幌地下街ポールタウン内',
  //   '011-271-5153',
  //   '500台有料',
  //   '○',
  //   '10:00〜20:00',
  //   '年中無休',
  //   '－',
  //   '○',
  //   '○',
  //   '－',
  //   '－',
  //   '－',
  //   '○',
  //   '○',
  //   '－',
  //   '－',
  //   '－',
  //   '－' ],
  const fnShopDetail = async function(page, shopURL, url) {
    console.log("fnShopdetail working.....");
    let shopname_pos = []


    let maplocation = await page.$$eval('.floatR.mt20res.mapConfirmation a.link.linkGra.linkExternal', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].href;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(maplocation);

    var qurl = url.parse(maplocation[0],true)
    var queryarray = qurl.query

    //基本情報、営業時間
    let bulkdatarow1 = await page.$$eval('div.flex6.spFlex12:nth-child(1) div.hgText tbody tr th', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarow1);
    let bulkdatarowd1 = await page.$$eval('div.flex6.spFlex12:nth-child(1)  div.hgText tbody tr td', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarowd1);
    //サービス情報
    let bulkdatarow2 = await page.$$eval('div.flex6.spFlex12:nth-child(2) div.hgText tbody tr th', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarow2);
    let bulkdatarowd2 = await page.$$eval('div.flex6.spFlex12:nth-child(2) div.hgText tbody tr td', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdatarowd2);

    let location = await page.$$eval('#location', (list,surl) => {
      var datas=[];
      if (list.length == 0) return "";
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas[0];
    },shopURL);
    console.log(location);

    let bulkdata = await page.$$eval('tbody tr td', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent //.split('\n')[0];
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkdata);

    shopname_pos.push(bulkdata[2])  //postal
    shopname_pos.push(bulkdata[2])  //address
    shopname_pos.push(bulkdata[3])  //tel1
    shopname_pos.push(bulkdata[3])  //tel2
    shopname_pos.push("")  //open
    shopname_pos.push("")  //holiday
    for (let i = 0; i < bulkdatarow1.length; i++) {
      if (bulkdatarow1[i] == "営業時間") shopname_pos[4] = (bulkdatarowd1[i]);  //open
      if (bulkdatarow1[i] == "定休日") shopname_pos[5] = (bulkdatarowd1[i]);  //open
    }
    for (let i = 0; i < bulkdatarow2.length; i++) {
      shopname_pos.push( bulkdatarow2[i] + ":" + bulkdatarowd2[i])
    }

    let bulktel = await page.$$eval('table.tbl tbody tr td.bgWh a.tel', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulktel);
    if (bulktel.length > 0) {
      shopname_pos[2] = bulktel[0]
      shopname_pos[3] = bulktel[0]
    }

    let bulkaddr = await page.$$eval('tr[id="postal_code address"] td.bgWh span', (list,surl) => {
      var datas=[];
      if (list.length == 0) return false;
      for (let i = 0; i < list.length; i++) {
        var data =  list[i].textContent;
        datas.push(data);
      }
      return datas;
    },shopURL);
    console.log(bulkaddr);
    if (bulkaddr.length > 0) {
      var data = ""
      for (let i = 0; i < bulkaddr.length; i++) {
        data = data + bulkaddr[i];
      }
      let postal = shopname_pos[0].split(data)[0].split('〒')[1]
      shopname_pos[0] = postal
      shopname_pos[1] = data
      shopURL.prefn = bulkaddr[0]
      shopURL.prefa = bulkaddr[1]
    }

    console.log(shopname_pos);

    let detail =  {
      shopdetail: {
        name: shopURL.textContent, 
        lat: queryarray['lat'],
        lon: queryarray['lon'],
        location: location,        
        contents: shopname_pos, 
        prefn: shopURL.prefn, 
        prefa:shopURL.prefa, 
        pref: shopURL.pref, 
        href: shopURL.href,
        id: shopURL.id[shopURL.id.length-2]
      }
    }
    console.log(detail);
    return detail
  }


  let areadatas
  let shopdetails=[]

  for (currentURL of PREFS) {
    const allResultsSelector = '.bgWh.box li a.link.linkGra';

    await page.goto(TOP_URL + 'search/pref/' + currentURL + '/', {waitUntil: 'networkidle2'}); //default 1000
    //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

    const prefname = await page.evaluate((selector) => {
      const sel = document.querySelector(selector)
      if (sel == null) return null
      return document.querySelector(selector).textContent
    }, 'h1.hdg-l1 strong[itemprop="addressRegion"]');


    const title = await page.title().then( text => {
      console.log("############# PREF text="+text)
      return text;
    });

    //await page.screenshot({path: currentURL + title + 'home2.png', fullPage: true})

    //const allResultsSelector2 = '.sec-cmn-lv3 .mod-shop-map.only-pc .item.s-'+currentURL+' .list a';
    const shops = await page.$$eval('.bgWh.box li a.link.linkGra', (list,prefname,prefid) => {
      let shops=[];
      for (let i = 0; i < list.length; i++) {
        let urlq = list[i].href.split("/");
        const shop = {
          prefn: prefname,
          prefa: "",
          pref: prefid,
          href: list[i].href,
          id: urlq,
          textContent: list[i].textContent,
          innerHTML: list[i].innerHTML
        };
        shops.push(shop);
      }
      return shops;
    },prefname,currentURL);

    console.log(shops);

    const sleepNotify = () => {
      console.log('wait sleeping')
    }
    const sleep = (waitSeconds, someFunction) => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(someFunction())
        }, waitSeconds * 1000)
      })  
    }

    for (shopdURL of shops) {
      sleep(3,sleepNotify)
      let gotopage=false
      while (!gotopage) {
        try {
          await page.goto(shopdURL.href);
          gotopage = true;
        }catch(err){
          console.log(err)
        }

      }
      //await page.waitForNavigation({timeout: 120000, waitUntil: "domcontentloaded"});

      //詳細ページのパンくずからエリアを取得
      shopdURL.prefa = ""
      // await page.$$eval('#topicpath-area ol.topicpath li', list => {
      //   var datas=[];
      //   if (list.length == 0) return false;
      //   for (let i = 0; i < list.length; i++) {
      //     if(i===4) {
      //       datas.push(list[i].textContent);
      //     }
      //   }
      //   if (datas.length > 0) return datas[0];
      //   return "";
      // });

      //await page.screenshot({path: shopdURL.prefn + '_' + shopdURL.prefa + '_' + shopdURL.textContent + '_' + '_detail.png', fullPage: true})

      const detail = await fnShopDetail(page,shopdURL,aurl)
      if (detail !== false) {
        fnWriteData("./test-s.csv",fnConvArray(detail.shopdetail));
        shopdetails.push(detail);
      }

    }


  }
  //console.log(shopdetails);

  browser.close();

  //assert(fs.existsSync('home1.png'));

  console.log('### Helooooooo');
})();
} catch (err) {
  console.error(err)
}



