build image

```
cd (repo root)

docker build -f 7.0/Dockerfile -t testchrome:latest .
```

run container

```
docker run -d -it testchrome:latest
```
 or

```
 docker run -d -it testchrome:latest ash
```

run container with mount volume

```
docker run -d -v `pwd`/output:/output -it testchrome:latest
```

connect container with new session

(xxx) check by `docker ps`

```
docker exec -it (xxx) ash

cd /output

ls
```

let's check test-a.csv
