#!/bin/bash
IMAGE=shopcrawler:letest

mkdir crawlwork

git clone https://hkd1123@bitbucket.org/hkd1123/docker-crawler.git ./crawlwork

CPATH=`pwd`

cd $CPATH/crawlwork/

WRKPATH=`pwd`

echo 'building image'

CIID=`docker build -f 7.0/Dockerfile -t $IMAGE .`

mkdir output

touch `pwd`/output/$1

CMD="docker run -d -v `pwd`/output:/output $IMAGE"
CID=`$CMD`

echo 'running container'
echo $CID

# example)
# bash run-crawler a.txt